﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeoBenny.models
{
    class Person
    {
        public int ID { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public double Age { get; set; }
        public decimal Salary { get; set; }
        public double WorkedMonths { get; set; }

        public Person():this("", "", 0, 0.0m, 0) { }
        public Person(string fn, string ln, double age, decimal salary, double workedmonths)
        {
            this.Firstname = fn;
            this.Lastname = ln;
            this.Age = age;
            this.Salary = salary;
            this.WorkedMonths = workedmonths;
        }

        public decimal YeaerlyIncome()
        {
            return Salary * 14;
        }

        public double WorkedInLifePercentage()
        {
            return WorkedMonths / Age / 12 * 100;
        }

        public override string ToString()
        {
            return Firstname + " " + Lastname + " " + Age;
        }
    }
}
