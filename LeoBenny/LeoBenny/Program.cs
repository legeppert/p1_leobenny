﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LeoBenny.models;

namespace LeoBenny
{
    class Program
    {
        static void Main(string[] args)
        {
            Person leo = new Person("Leonard", "Geppert", 17, 1000000m, 2);

            //Dieses Kommentar erzeugt einen Konflikt

            //noch ein kommentar => Konflikt

            Console.WriteLine(leo.YeaerlyIncome());

            Person benny = new Person("Benny", "Egger", 18, 7000m, 5);

            Console.WriteLine("Benny's WorkedLifePercentage: "+benny.WorkedInLifePercentage());
            Console.WriteLine("--Benny");

            Console.ReadKey();
        }
    }
}
